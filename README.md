## Problems

* Makes the API server certificate expire at a specific time.
* Creates a resolv.conf file to an incorrect location and updates it to use 127.0.0.1
* Adds a /etc/motd for help message
* Creates an annoying cron message with `wall`

## Break the cluster

#### Break CCM

Modify the [CCM](https://github.com/equinix/cloud-provider-equinix-metal) secret and delete the `token`

```
kubectl get secrets -n kube-system

kubectl edit secrets -n kube-system cloud-controller-manager-token-mdsvv
```

#### Break DNS

Change kubelet config map to use modified resolv.conf file

replace `resolvConf: /run/systemd/resolve/resolv.conf` with `resolvConf: /run/systemd/resolv.conf`

```
kubectl edit configmap -n kube-system kubelet-config-1.19
```

#### Create support docker image

Push the image to any container registry you want.

```
docker build .
```

#### Create certificates

Download `/etc/kubernetes/pki/ca.*` from control plane server put them in certs/

change ca-config.json so the expiration date will make the certificates expire when you want them to.

```
cd certs
./create-apiserver-cert.sh
mv apiserver.pem apiserver.crt
mv apiserver-key.pem apiserver.key
```

### Run playbook

```
ansible-playbook -i inventory.txt playbook.yaml
```
