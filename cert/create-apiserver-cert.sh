#!/bin/bash

HOSTNAMES="kluster-003-control-plane-8wnvt,kubernetes,kubernetes.default,kubernetes,default.svc,kubernetes.default.svc.cluster.local,172.26.0.1,145.40.77.179,145.40.77.90,145.40.77.247"

cfssl gencert -ca ca.crt \
		-ca-key ca.key -config ca-config.json \
		-hostname ${HOSTNAMES} \
		-profile apiserver apiserver-csr.json \
		| cfssljson -bare apiserver
